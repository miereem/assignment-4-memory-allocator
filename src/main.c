#include <stddef.h>
#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

// #include <stddef.h>
// #define _DEFAULT_SOURCE
// #include <unistd.h>

// #include "mem_internals.h"
// #include "mem.h"
// #include "util.h"


#define INITIAL_HEAP_SIZE 10240


extern void debug(const char *fmt, ...);


static void* test_heap_init() {
    debug("Heap initializing started\n");
    void *heap = heap_init(INITIAL_HEAP_SIZE);
    if (heap == NULL) {
        printf("Cannot initialize heap");
    }
    printf("Heap initialized successfully\n\n");
    return heap;
}



void failmsg() {
    printf("FAIL!\n");
}

void succmsg() {
    printf("SUCCESS!\n\n");
}


void test1 (struct block_header *first_block) {
    debug("Test on memory allocation has started:\n");

    const size_t test_size = 524;

    void *mem = _malloc(test_size);

    debug_heap(stdout, first_block);

    bool ans = (first_block->capacity.bytes == test_size && mem != NULL);

    if (!ans) {
       failmsg();
    }
    else {
        succmsg();
    }
    _free(mem);
}

static inline struct block_header* get_block_by_allocated_mem(void* mem) {
    return (struct block_header *) ((uint8_t *) mem - offsetof(struct block_header, contents));
}

void test2(struct block_header *first_block) {
    debug("Test on block freeing has started:\n");

    void *mem1 = _malloc(5545), *mem2 = _malloc(600);

    if (mem1 == NULL || mem2 == NULL) {
        failmsg();
    }

    _free(mem1);

    debug_heap(stdout, first_block);

    struct block_header *mem_block1 = get_block_by_allocated_mem(mem1), *mem_block2 = get_block_by_allocated_mem(mem2);

    if (!mem_block1->is_free || mem_block2->is_free) {
        failmsg();
    }
  
    succmsg();

    _free(mem1);
    _free(mem2);
}

void test3(struct block_header *first_block) {
    debug("Test on freeing multiple blocks has started:\n");

    void *mem1 = _malloc(1024), *mem2 = _malloc(1024), *mem3 = _malloc(1024);

    if (mem1 == NULL || mem2 == NULL || mem3 == NULL) {
       failmsg();
    }

    _free(mem2);
    _free(mem1);

    debug_heap(stdout, first_block);

    struct block_header *mem_block1 = get_block_by_allocated_mem(mem1), *mem_block3 = get_block_by_allocated_mem(mem3);
    if (!mem_block1->is_free || mem_block3->is_free || mem_block1->capacity.bytes != 2048 + offsetof(struct block_header, contents)) {
        failmsg();
    }

   succmsg();

    _free(mem1);
    _free(mem2);
    _free(mem3);
}

void test4(struct block_header *first_block) {

    debug("Test on expanding memory has started:\n");

    void *mem1 = _malloc(INITIAL_HEAP_SIZE), *mem2 = _malloc(INITIAL_HEAP_SIZE + 512), *mem3 = _malloc(2048);

    if (mem1 == NULL || mem2 == NULL || mem3 == NULL) {
        failmsg();
    }

    _free(mem3);
    _free(mem2);

    debug_heap(stdout, first_block);

    struct block_header *mem_block1 = get_block_by_allocated_mem(mem1), *mem_block2 = get_block_by_allocated_mem(mem2);

    if ((uint8_t *)mem_block1->contents + mem_block1->capacity.bytes != (uint8_t*) mem_block2){
       failmsg();
    }
 succmsg();

    _free(mem1);
    _free(mem2);
    _free(mem3);
}



void test5(struct block_header *first_block) {
    debug("Test on expanding new region of memory has started:\n");

    void *mem1 = _malloc(11250);

    if (mem1 == NULL) {
       failmsg();
    }

    struct block_header *addr = first_block;

    while (addr->next) addr = addr->next;

    void*  tmp = first_block->contents + first_block->capacity.bytes;
    void*  mtmp = mmap(tmp, 256, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    
    if (mtmp == NULL) {
    	failmsg();
    }

    void *mem2 = _malloc(100000);

    debug_heap(stdout, first_block);

    struct block_header *mem2_block = get_block_by_allocated_mem(mem2);
    if (mem2_block == addr) {
       failmsg();
    }
     succmsg();

    _free(mem1);
    _free(mem2);
}

int main(){
    struct block_header *first_block = (struct block_header*) test_heap_init();

    test1(first_block);
    test2(first_block);
    test3(first_block);
    test4(first_block);
    test5(first_block);

    printf("All tests passed.\n\n");

    return 0;
}
